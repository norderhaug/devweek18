(ns app.crypto.view
  (:require
   [goog.string :as gstring]
   [reagent.core :as reagent
     :refer [atom]]
   [re-frame.core :as rf]
   [cljsjs.material-ui]
   [cljs-react-material-ui.core :as material
     :refer [get-mui-theme color]]
   [cljs-react-material-ui.reagent :as ui]
   [cljs-react-material-ui.icons :as ic]
   [sdk.pubnub :as pubnub
    :refer [pubnub]]
   [sdk.eon :as eon]
   [app.crypto.panel
    :refer [panel]]))

(def card-style {:color (color :blue-grey50)
                 :background-color (color :blue-grey800)})

(defn toolbar [{:keys [] :as session}]
  [ui/app-bar
   {:on-left-icon-button-touch-tap #()
    :on-right-icon-button-touch-tap #()
    :title (reagent/as-element
            [:div "AlphaBayesCrypto"])}])

(defn footer [session]
  [:div {:style {:margin-top "1em"}}
   [ui/card {:style card-style}
    [ui/card-header
     [:h3 {:style {:text-align "center"
                   :color "white"}}
      "Powered by "
      "Tensorflow for Bayesian Modelling and Probabilistic Programming"
      #_"Clojure - "
      #_"Python - "
      #_"React - "
      #_"PubNub"]]]])


(defn view [{:keys [] :as session}]
  [ui/mui-theme-provider
   {:mui-theme (get-mui-theme
                {:palette
                 {:primary1-color (color :blue-grey800)
                  :primary2-color (color :blue-grey500)
                  :primary3-color (color :blue-grey300)
                  :alternate-text-color (color :blue-grey50) ;; used for appbar text
                  :primary-text-color (color :light-black)}})}
   [:div {:style {:background-color (color :blue-grey900)
                  :min-height "100vh"
                  :margin "-2em"
                  :padding "2em"
                  :padding-top "2.5em"}}
     [toolbar session]
     [panel session]
     [footer session]]])
