(ns app.crypto.bayes
  (:require
   [goog.string :as gstring]
   [reagent.core :as reagent
     :refer [atom]]
   [re-frame.core :as rf]
   [cljsjs.material-ui]
   [cljs-react-material-ui.core :as material
     :refer [get-mui-theme color]]
   [cljs-react-material-ui.reagent :as ui]
   [cljs-react-material-ui.icons :as ic]
   [sdk.pubnub :as pubnub
    :refer [pubnub]]
   [sdk.eon :as eon]))

(def card-style {:color (color :blue-grey50)
                 :background-color (color :blue-grey800)})

(defn donut-chart [{:keys [id channel]}]
  [eon/chart {:channels [channel]
              :id id
              :pubnub pubnub
              :generate {:bindto (str "#" id)
                         :data {:labels true
                                :type "donut"}}}])

(defn view [{:keys [] :as session}]
  [ui/card {:style card-style}
   [ui/card-header
    [:h3 {:color "white"}
     "Twitter Sentiments"]]
   [ui/card-text
    [donut-chart {:id "sentiments1"
                  :channel "sentiments"}]]])
