(ns app.crypto.panel
  (:require
   [goog.string :as gstring]
   [reagent.core :as reagent
     :refer [atom]]
   [re-frame.core :as rf]
   [cljsjs.material-ui]
   [cljs-react-material-ui.core :as material
     :refer [get-mui-theme color]]
   [cljs-react-material-ui.reagent :as ui]
   [cljs-react-material-ui.icons :as ic]
   [sdk.pubnub :as pubnub
    :refer [pubnub]]
   [sdk.eon :as eon]
   [app.crypto.advise :as advise]
   [app.crypto.bayes :as bayes]))

(defn coinchart [{:keys [channel id low hi spread]}]
    [eon/chart {:channels [channel]
                :id id
                :history true ; true leads to error unless pubnub has enabled history!
                :flow true
                :limit 15
                :pubnub pubnub
                :generate {:bindto (str "#" id)
                           :data {:labels false :type "line"}
                           :axis {:y {:padding {:top (or hi (if (and low spread)
                                                              (* low spread)
                                                              :topPadding))
                                                :bottom (or low :bottomPadding)}
                                      :tick {:fit true}}
                                  :x {:type "timeseries"
                                      :tick {:format "%M:%S" #_"%H:%M:%S"
                                             :fit true}}}}}])

(defn crypto-row [{:keys [] :as session}]
  [:div.row {:style {:margin-top "0.5em"
                     :min-height "12em"}}
   #_
   [:div.col-lg-4
    [:div.panel.panel-default
     [:div.panel-body
      [coinchart {:id "bitcoinChart" :channel "bitcoin-feed"
                  :low 7000 :spread 1.3}]]]]
   [:div.col-lg-4
    [ui/card {:style {:background-color (color :blue-grey800)}}
     [ui/card-text
      [coinchart {:id "bitcoinChart" :channel "bitcoin-feed"
                  :low 7000 :spread 1.3}]]]]
   [:div.col-lg-4
    [ui/card {:style {:background-color (color :blue-grey800)}}
     [ui/card-text
      [coinchart {:id "etherChart" :channel "ether-feed"
                  :low 800 :spread 1.3}]]]]
   [:div.col-lg-4
    [ui/card {:style {:background-color (color :blue-grey800)}}
     [ui/card-text
      [coinchart {:id "liteCoinChart" :channel "litecoin-feed"
                  :low 150 :spread 1.3}]]]]])

(defn panel [{:keys [] :as session}]
  [:div
   [crypto-row session]
   [:div.row {:style {:margin-top "1em"}}
    [:div.col-lg-4
     [bayes/view session]]
    [:div.col-lg-8
     [advise/view session]]]])
