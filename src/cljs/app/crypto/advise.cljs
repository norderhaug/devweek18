(ns app.crypto.advise
  (:require
   [goog.string :as gstring]
   [reagent.core :as reagent
     :refer [atom]]
   [re-frame.core :as rf]
   [cljsjs.material-ui]
   [cljs-react-material-ui.core :as material
     :refer [get-mui-theme color]]
   [cljs-react-material-ui.reagent :as ui]
   [cljs-react-material-ui.icons :as ic]
   [sdk.pubnub :as pubnub
    :refer [pubnub]]
   [sdk.eon :as eon]))

(def card-style {:color (color :blue-grey50)
                 :background-color (color :blue-grey800)})

(defn gauge-chart [{:keys [id channel]}]
  [eon/chart {:channels [channel]
              :id id
              :pubnub pubnub
              :generate {:bindto (str "#" id)
                         :gauge {:min 0 :max 100}
                         :color {:pattern ["#FF0000" "#F6C600" "#60B044"]}
                         :threshold {:values [20 40 60]}
                         :data {:labels true
                                :type "gauge"}}}])

(defn view [{:keys [] :as session}]
  [ui/card {:style card-style}
   [ui/card-header
    [:h3 {:color "white"}
     "BTC Purchase Confidence"]]
   [ui/card-text
    [gauge-chart {:id "predict1"
                  :channel "predict"}]]])
