(ns app.arrive
  (:require-macros
   [cljs.core.async.macros
    :refer [go go-loop]])
  (:require
   [cljs.core.async :as async
    :refer [chan <! >! put! close! timeout promise-chan]]
   [taoensso.timbre :as timbre]
   [re-frame.core :as rf]
   [sdk.curbside :as curbside]))

(def app-id "aidintime-app")
(def usage-token "a7297c199c17d7c5754590259a121d4b82f1958600d6e3f3931f54bc5a3dd130")
(def site-id "aidintime_disaster")
(def track-token "user1")


(defn init []
  (timbre/debug "Init Curbside Arrive")

  (curbside/set-app-id app-id)
  (curbside/set-usage-token usage-token)
  (curbside/set-tracking-id track-token)

  (rf/reg-event-db
   :arrive/go-pickup
   (fn [db _]
     (timbre/info "Start Trip")
     (curbside/start-trip {:site-id site-id
                           :track-token track-token})
     (assoc db :stage :pickup
               :arrive/go-pickup true))))
