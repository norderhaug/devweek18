(ns app.data
  (:require
   [camel-snake-kebab.core
    :refer [->kebab-case-keyword]]
   [camel-snake-kebab.extras
    :refer [transform-keys]]))

(defn transform-kebab-keys [m]
  (transform-keys ->kebab-case-keyword m))

(def state
  {:brand "Aid in Time"
   :mode (case :default
           :crypto "crypto"
           :split "split"
           nil)
   :stage "diagnostic"
   :mobile {:stage nil}
   :panes [{:id "about" :title "About"}
           {:id "main" :title "Main"}
           {:id "info" :title "Info"}]
   :pane "info"})
