(ns app.view.view
  (:require-macros
   [kioo.reagent
    :refer [defsnippet deftemplate snippet]])
  (:require
   [kioo.reagent
    :refer [html-content content append after set-attr do->
            substitute listen unwrap]]
   [reagent.core :as reagent
    :refer [atom]]
   [app.crypto.view :as crypto]
   [app.mobile.core :as mobile]
   [app.dashboard.core :as dashboard]))

(defn split-view [session]
  [:div
    [:div {:style {:width "30%"
                   :float "left"}}
      [:div.phone
        [:div.phone-screen
          [mobile/view session]]]]
    [:div {:style {:width "60%"
                   :height "100vh"
                   :border-left "thin solid gray"
                   :float "right"}}
      [dashboard/view session]]])

(defn loading-view [session]
  [:div {:style {:margin-top "5em" :padding "3em"}}
   #_
   [:div {:style {:margin-top "5em" :padding "3em"}}
    [:div.progress
     [:div.progress-bar.progress-bar-animated
      {:role "progress-bar"
       :aria-valuenow "100"
       :aria-valuemin "0"
       :aria-valuemax "100"
       :style {:width "100%"}}]]]
   [:ul.list-group
    [:a.list-group-item {:href "/#crypto"}
      "AlphaBayesCrypto"]
    [:a.list-group-item {:href "/#mobile"}
      "Mobile"]
    [:a.list-group-item {:href "/#dashboard"}
      "Dashboard"]]])

(defn view [{:keys [mode] :as session}]
  (case (if mode @mode)
    ("mobile")
    [mobile/view session]
    ("dashboard")
    [dashboard/view session]
    ("split")
    [split-view session]
    ("crypto")
    [crypto/view session]
    (nil)
    [loading-view session]))
