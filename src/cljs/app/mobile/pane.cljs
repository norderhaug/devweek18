(ns app.mobile.pane
  (:require
   [reagent.core :as reagent
    :refer [atom]]
   [re-frame.core :as rf]
   [cljsjs.material-ui]
   [cljs-react-material-ui.core :as material
    :refer [get-mui-theme color]]
   [cljs-react-material-ui.reagent :as ui]
   [cljs-react-material-ui.icons :as ic]
   [goog.string :as gstring]
   [app.arrive :as arrive]))

(defmulti pane (fn [{:keys [stage] :as session}]
                  (if stage [@stage])))

(defn family-size-selector [{:keys [family-size] :as session}]
  [ui/slider {:value (or (if family-size @family-size) 1)
              :on-change (fn [e val]
                           (rf/dispatch [:family-size val]))
              :min 1
              :max 9
              :step 1}])

(defn step [{:keys [label final step-index]} content]
  [ui/step
   [ui/step-label label]
   [ui/step-content {}
    content]
   [ui/step-content {:style {:margin-top "1em"}}
    [:div {:style {:display (if final "none")}}
      [ui/flat-button {:label "Next"
                       :on-click #(swap! step-index inc)}]]]])

(defn stepper [{:keys [mobile family-size step-index]
                :as session}]
  [ui/stepper {:active-step (if step-index @step-index 0)
               :orientation "vertical"}
   (step {:label "Name" :step-index step-index}
     [ui/text-field
      {:hint-text "Type in your name"}])
   (step {:label "Group Size"  :step-index step-index}
     [family-size-selector session])
   (step {:label "Get Supplies"
          :final true
          :step-index step-index}
     [ui/raised-button
      {:label "Submit Request"
       :on-click #(rf/dispatch [:arrive/go-pickup])}])])

(defn pane1 [session]
  (let [index (atom 0)]
    (fn [session]
      [pane (assoc session :step-index (atom 0))])))

(defmethod pane :default [{:keys [mobile family-size] :as session}]
  [ui/card
   [ui/card-text
    [stepper (assoc session :step-index (atom 0))]]])
