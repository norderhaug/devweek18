(ns app.mobile.pickup
  (:require
   [goog.string :as gstring]
   [reagent.core :as reagent
     :refer [atom]]
   [re-frame.core :as rf]
   [cljsjs.material-ui]
   [cljs-react-material-ui.core :as material
     :refer [get-mui-theme color]]
   [cljs-react-material-ui.reagent :as ui]
   [cljs-react-material-ui.icons :as ic]
   [sdk.pubnub :as pubnub
    :refer [pubnub]]
   [sdk.eon :as eon]
   [app.mobile.pane
    :refer [pane]]))

#_
(defn map-chart [{:keys [channel id]}]
  [eon/mapbox {:channels [channel]
               :id id
               :pubnub pubnub}])

(defmethod pane [:pickup] [session]
  [:div.alert.alert-info {:style {:margin "5em"}}
   "Supplies Ready for Pickup"])
