(ns sdk.curbside
  (:require-macros
   [cljs.core.async.macros
    :refer [go go-loop]])
  (:require
   [cljs.core.async :as async
    :refer [chan <! >! put! close! timeout promise-chan]]
   [taoensso.timbre :as timbre]))

;; https://developer.curbside.com/docs/getting-started/quickstart-web-app/

(def dependencies-script
  {:script "https://hosted.curbside.com/1.0/curbside.dependencies.js"})

(def sdk-script
  {:script "https://hosted.curbside.com/1.0/curbside.sdk.js"})

(defn curbside []
  (when-not (exists? js/Curbside)
    (timbre/warn "Curbside SDK not loaded"))
  js/Curbside)

(defn set-app-id [id]
  (.setAppId (curbside) id))

(defn set-usage-token [token]
  (.setUsageToken (curbside) token))

(defn set-tracking-id [id]
  (.setTrackingIdentifier (curbside) id))

(defn start-trip [{:keys [site-id track-token]}]
  (-> (curbside)
      (.startTripToSiteWithIdentifier
       #js{:siteID site-id :trackToken track-token})
      (.then #(timbre/info "Trip estimates:" (js->clj (.-estimates %))))
      (.catch #(timbre/error "Failed to start trip:" (.-message %)))))

(defn cancel-trip [{:keys [site-id track-token]}]
  (-> (curbside)
      (.cancelTripToSiteWithIdentifier
       #js{:siteID site-id :trackToken track-token})
      (.then #(timbre/info "Canceled trip"))
      (.catch #(timbre/error "Failed to cancel trip:" (.-message %)))))
