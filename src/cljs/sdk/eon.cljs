(ns sdk.eon
  (:require
   [reagent.core :as reagent
    :refer [atom]]))

(def script {:src "//pubnub.github.io/eon/v/eon/1.0.0/eon.js"})

(defn eon []
  (if (exists? js/eon)
    js/eon))

#_
(eon)

(defn enable-chart
  ([eon config]
   {:pre [(some? eon)]}
   (.chart eon (clj->js config))))

(defn chart
  ([config]
   [chart {:style {:width "100%"
                   :min-height "15em"}}
          config])
  ([attr config]
   {:pre [(:id config)]}
   (reagent/create-class
    {:component-did-mount #(enable-chart (eon) config)
     :reagent-render (fn [attr config]
                       [:div (assoc attr :id (:id config))])})))
