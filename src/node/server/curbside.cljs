(ns server.curbside
  (:require-macros
   [cljs.core.async.macros :as m
    :refer [go go-loop alt!]])
  (:require
   [cljs.core.async :as async
    :refer [chan close! timeout put!]]
   [sdk.curbside :as curbside]))

(defn handler [req res]
  (go
    (.set res "Content-Type" "text/html")
    (.send res "OK")))
