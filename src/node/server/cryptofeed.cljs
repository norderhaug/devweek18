(ns server.cryptofeed
  (:require-macros
   [cljs.core.async.macros
    :refer [go go-loop]])
  (:require
   [cljs.core.async :as async
    :refer [chan <! >! put! close! timeout promise-chan]]
   [cljs-http.client :as http]
   [taoensso.timbre :as timbre]
   [sdk.pubnub :as pubnub]
   [sdk.cryptocompare :as cryptocompare]))

(defn fixed [num dig]
  (if (number? num)
    (.toFixed num dig)
    num))

(defn eon-crypto-message [msg label coin]
  {:eon {label (:USD (fixed (coin msg) 2))}})

(defn process-request [req]
  (timbre/info "=>" req)
  (pubnub/publish {:channel "crypto" :message req})
  (doseq [{:keys [channel label coin]}
          [{:channel "bitcoin-feed"
            :label "BitCoin" :coin :BTC}
           {:channel "ether-feed"
            :label "Ether" :coin :ETH}
           {:channel "litecoin-feed"
            :label "LiteCoin" :coin :LTC}]
          :let [message (eon-crypto-message req label coin)]]
    #_(timbre/info "Crypto Message:" message)
    (pubnub/publish {:channel channel
                     :message message})))

#_
(go (-> (<! (cryptocompare/fetch-crypto-prices))
        (process-request)))

(defn crypto-server [delay]
  (go-loop []
    (let [msg (<! (cryptocompare/fetch-crypto-prices))]
      (process-request msg)
      (<! (timeout (* delay 1000)))
      (recur))))

(defn unleash-twitter []
  (pubnub/add-listener
   pubnub/twitter-pubnub
   {:message (fn [msg]
               (timbre/info "TWITTER:" (js->clj msg)))})
  (pubnub/subscribe
   pubnub/twitter-pubnub
   {:channels #js["pubnub-twitter"]}))

#_
(unleash-twitter)
